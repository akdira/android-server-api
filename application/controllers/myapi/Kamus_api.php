<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class Kamus_api extends REST_Controller {
    function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

    //Menampilkan data myuser
    function index_get() {
        
        $id = $this->get('id');
        if (!($id)) {
        	//$myuser = $this->db->get('mykamus')->result();
            $myuser = $this->db->query('select * from mykamus order by id desc')->result();
        } else {
            $this->db->where('id', $id);
            $this->db->from('mykamus');
            $this->db->order_by('id','desc');
            
            $myuser = $this->db->get()->result();
        }
        //print_r($this->response);exit;
        $this->response($myuser, 200);
    }

    //Mengirim atau menambah data users baru
    function index_post() {
        
        // Jika belum login maka tolak akses
        if ($this->output->get_output()!=null) {
            return;
        }
        
        $data = array(
                    'id'          => $this->post('id'),
                    'word'    => $this->post('word'),
                    'description'    => $this->post('description'));
        $insert = $this->db->insert('mykamus', $data);
        if ($insert) {
            $this->response($data, 200);
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }

//Memperbarui data users yang telah ada
    function index_put() {
        $id = $this->put('id');
        $data = array(
                    'id'         => $this->put('id'),
                    'word'   => $this->put('word'),
                    'description'   => $this->put('description'));
        $this->db->where('id', $id);
        $update = $this->db->update('mykamus', $data);
        if ($update) {
            $this->response($data, 200);
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }

//Menghapus salah satu data kontak
    function index_delete() {
        $id = $this->delete('id');
        $this->db->where('id', $id);
        // $this->db->where('id', 629);
       $delete = $this->db->delete('mykamus');
       // $delete = $this->db->query("delete from mykamus where id = $id");
        if ($delete) {
            $this->response(array('status' => 'success'), 201);
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }

}
?>