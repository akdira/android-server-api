<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class Auth_api extends REST_Controller {

    protected $CI;

    function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
        $this->load->model('User','',TRUE);
    }
    
    function login_post()
    {

        $username = $this->input->post('username');
        $password = $this->input->post('password');
        
        // If username is null or white space, maka validasi
        if(empty($username) or trim($username)==''){
            $data['status'] = false;
            $data['error'] = 'Please input username';
            
            return $this->response($data, 200);
            
        }

        // If password is null or white space, maka validasi
        if(empty($password) or trim($password)==''){
            $data['status'] = false;
            $data['error'] = 'Please input password';

            return $this->response($data, 200);
        }
        
        // Panggil fungsi cek ke database
        $is_valid = $this->check_database($username, $password);

        if($is_valid){
            $data['status'] = true;
            $data['error'] = 'Login success';
            
            return $this->response($data, 200);
        }
        else{
            $data['status'] = false;
            $data['error'] = 'Invalid username or password';
            
            return $this->response($data, 200);

        }
    
    }

    function logout_get()
    {
        $data['status'] = false;
        $data['error'] = 'Logout successfully';

        $this->session->unset_userdata('logged_in');
        session_destroy();
            
        return $this->response($data, 200);
    }
        
    private function check_database($username, $password)
    {
      //Field validation succeeded.  Validate against database
      //query the database
      $result = $this->User->login_api($username, $password);

      if($result)
      {
        $sess_array = array();
        foreach($result as $row)
        {
          $sess_array = array(
            'id' => $row->id,
            'username' => $row->username
          );
          $login_data = array( 'logged_in' => $sess_array );
          $this->session->set_userdata($login_data);
        }
        return TRUE;
      }
      else
      {
        return false;
      }
    }
}