<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class AuthLib {

    protected $CI;

    function __construct()
    {
      $this->CI =& get_instance();
      $this->CI->load->model('User','',TRUE);
    }
    
    function login($username, $password)
    {

        
        // If username is null or white space, maka validasi
        if(empty($password) or trim($username)==''){
            return false;
        }

        // If password is null or white space, maka validasi
        if(empty($password) or trim($password)==''){
            return false;
        }
        
        // Panggil fungsi cek ke database
        $is_valid = $this->check_database($username, $password);
        
        return $is_valid;
    
    }
    
    function check_database($username, $password)
    {
      //Field validation succeeded.  Validate against database
      //query the database
      $result = $this->CI->User->login($username, $password);
    
      if($result)
      {
        $sess_array = array();
        foreach($result as $row)
        {
          $sess_array = array(
            'id' => $row->id,
            'username' => $row->username
          );
          $login_data = array( 'logged_in' => $sess_array );
          $this->CI->session->set_userdata($login_data);
        }
        return TRUE;
      }
      else
      {
        return false;
      }
    }
}